﻿namespace Blog.Controllers
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    using BlogModel.Models;
    using BlogModel.ViewModel;
    using BlogServices.User;
    using BlogServices.Utils;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using Microsoft.IdentityModel.Tokens;

    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ApplicationSettings _appSettings;

        public UserController(IUserService userService, IOptions<ApplicationSettings> appSettings)
        {
            _userService = userService;
            _appSettings = appSettings.Value;
        }

        private string GenerateToken(Users user)
        {
            var tokenDescriptor = new SecurityTokenDescriptor
            {

                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("UserId", user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(90),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.JWT_Secret)),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);
            return token;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] UserViewModel userViewModel)
        {
            try
            {
                if (userViewModel == null)
                {
                    return BadRequest("Invalid model data");
                }

                Users users = new Users();
                users.Email = userViewModel?.Email;
                users.UserName = userViewModel?.UserName;
                users.Password = userViewModel?.Password;
                var userRep = await _userService.CreateUserAsync(users);

                if (userRep == null)
                {
                    return BadRequest("Duplicate User");
                }

                return Ok(userRep);

            }
            catch (Exception ex)
            {

                return StatusCode(500, new { Status = "Error", Message = "Registration failed", ex });
            }

        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel loginViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model data");
                }
                var usersData = await _userService.LoginUserAsync(loginViewModel);

                if (usersData == null)
                {
                    return BadRequest("Login Failed");
                }

                return Ok(new { token = GenerateToken(usersData) });
            }
            catch (Exception ex)
            {

                return StatusCode(500, new { Status = "Error", Message = "Login Failed", ex });
            }
        }

        [Authorize]
        [HttpGet]
        [Route("[action]")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> GetUserTransactions(int userId)
        {
            try
            {
                var userTransaction = await _userService.GetUserWithTransactions(userId);
                if (userTransaction == null)
                {
                    return BadRequest("Failed to get UserTransaction");
                }
                return Ok(userTransaction);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Status = "Error", Message = "UserWith Transaction Failed", ex });
            }
        }
    }
}