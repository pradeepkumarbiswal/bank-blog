﻿

namespace Blog.Controllers
{
    using BlogModel.Utils;
    using BlogModel.ViewModel;
    using BlogServices.Transaction;
    using BlogServices.Utils;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }
        [Authorize]
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> Post([FromBody] TransactionViewModel transactionViewModel, int userId)
        {
            try
            {
                if (transactionViewModel.Balance < 0)
                {
                    return BadRequest("Amount should be more than 0");
                }

                if (transactionViewModel.Type != Convert.ToInt32(TransactionEnum.TransactionType.Deposit) && transactionViewModel.Type != Convert.ToInt32(TransactionEnum.TransactionType.Withdrawal))
                {
                    return BadRequest("Invalid transaction type.");
                }

                TransactionEnum.TransactionType transactionType =
                    transactionViewModel.Type == Convert.ToInt32(TransactionEnum.TransactionType.Deposit)
                    ? TransactionEnum.TransactionType.Deposit : TransactionEnum.TransactionType.Withdrawal;


                var transactionRes = await _transactionService.DepositeWithdrawAsync(userId, transactionViewModel.Balance, transactionType);

                if (transactionRes.Balance == 0)
                {
                    return BadRequest("Cannot withdraw more than available balance.");
                }

                return Ok(transactionRes);
            }
            catch (Exception)
            {
                return StatusCode(500, new { Status = "Error", Message = "Transaction Failed" });
            }
        }

        [Authorize]
        [HttpGet]
        [Route("GetBalance")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> GetBalance(int userId)
        {
            try
            {
                var getBalance = await _transactionService.GetCurrentBalanceAsync(userId);
                if (getBalance == null)
                {
                    return BadRequest("No Transaction Find");
                }
                return Ok(getBalance);
            }
            catch (Exception)
            {
                return StatusCode(500, new { Status = "Error", Message = "Transaction Failed" });
            }
        }

        [Authorize]
        [HttpGet]
        [Route("GetAllTransaction")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> GetAllTransaction(int userId)
        {
            try
            {
                var getTransaction = await _transactionService.GetAllTransactionAsync(userId);
                if (!getTransaction.Any())
                {
                    return BadRequest("No Transaction Find");
                }
                return Ok(getTransaction);
            }
            catch (Exception)
            {
                return StatusCode(500, new { Status = "Error", Message = "Transaction Failed" });
            }
        }

    }
}