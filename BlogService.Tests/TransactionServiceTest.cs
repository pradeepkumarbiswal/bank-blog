﻿namespace BlogService.Tests
{
    using BlogModel.Models;
    using BlogServices.Imp;
    using BlogServices.Transaction;
    using FakeItEasy;
    using SharedTestData;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Xunit;

    public class TransactionServiceTest
    {
        private readonly ITransactionRepository _fakeRepository;
        private readonly int userId = 1;

        public TransactionServiceTest()
        {
            _fakeRepository = A.Fake<ITransactionRepository>();
        }

        [Fact]
        public void GetAllTransaction_Test()
        {
            //Arrange
            var data = new TestData().TransactionData();
            A.CallTo(() => _fakeRepository.GetAllTransactionAsyncRepo(A<int>.Ignored)).Returns(data);
            ITransactionService transactionService = new TransactionService(_fakeRepository);

            //Act
            var transactionData = transactionService.GetAllTransactionAsync(userId).Result;

            //Assert
            Assert.Equal(transactionData, data);
        }

        [Fact]
        public void GetCurrentBalance_Test()
        {
            //Arrange
            var data = new TestData().TransactionData();
            A.CallTo(() => _fakeRepository.GetCurrentBalanceAsyncRepo(A<int>.Ignored)).Returns(data[0]);
            ITransactionService transactionService = new TransactionService(_fakeRepository);

            //Act
            var transactionData = transactionService.GetCurrentBalanceAsync(userId).Result;

            //Assert
            Assert.Equal(transactionData.Id, data[0].Id);
        }

        [Fact]
        public void DepositeWithdraw_Test()
        {
            //Arrange
            var data = new TestData().TransactionData();
            A.CallTo(() => _fakeRepository.DepositeWithdrawAsyncRepo(A<Transactions>.Ignored)).Returns(data[0]);
            ITransactionService transactionService = new TransactionService(_fakeRepository);

            //Act
            var transactionData = transactionService.DepositeWithdrawAsync(userId, 100, 0).Result;

            //Assert
            Assert.Equal(transactionData, data[0]);
        }

    }
}
