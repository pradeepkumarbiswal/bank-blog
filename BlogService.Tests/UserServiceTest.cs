namespace BlogService.Tests
{
    using BlogModel.Models;
    using BlogModel.ViewModel;
    using BlogServices.Imp;
    using BlogServices.User;
    using FakeItEasy;
    using SharedTestData;
    using System;
    using Xunit;
    public class UserServiceTest
    {
        private readonly IUserRepository _fakeRepository;
        private readonly int userId = 1;

        public UserServiceTest()
        {
            _fakeRepository = A.Fake<IUserRepository>();
        }

        [Fact]
        public void CreateUser_Test()
        {
            //Arrange
            var data = new TestData().Userdata();
            A.CallTo(() => _fakeRepository.CreateUserAsyncRepo(A<Users>.Ignored)).Returns(data);
            IUserService userService = new UserService(_fakeRepository);

            //Act
            var result = userService.CreateUserAsync(data).Result;

            //Assert
            Assert.Equal(result, data);
        }

        [Fact]
        public void DuplicateUser_Test()
        {
            //Arrange
            var data = new TestData().Userdata();
            A.CallTo(() => _fakeRepository.GetUserProfile(A<string>.Ignored)).Returns(data);
            A.CallTo(() => _fakeRepository.CreateUserAsyncRepo(A<Users>.Ignored)).Returns(data);
            IUserService userService = new UserService(_fakeRepository);

            //Act
            var result = userService.CreateUserAsync(data).Result;

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void LoginUser_Test()
        {
            //Arrange
            var data = new TestData().Userdata();
            A.CallTo(() => _fakeRepository.LoginUserAsyncRepo(A<LoginViewModel>.Ignored)).Returns(data);
            IUserService userService = new UserService(_fakeRepository);

            //Act
            var result = userService.LoginUserAsync(new LoginViewModel { Email = "pkbiswal321@gmail.com", Password = "12345" }).Result;

            //Assert
            Assert.Equal(result, data);
        }

        [Fact]
        public void GetUserTransaction_Test()
        {
            //Arrange
            var data = new TestData().UserWithTransaction();
            A.CallTo(() => _fakeRepository.GetUserWithTransactionsRepo(A<int>.Ignored)).Returns(data);
            IUserService userService = new UserService(_fakeRepository);

            //Act
            var result = userService.GetUserWithTransactions(userId).Result;

            //Assert
            Assert.Equal(result.Id, data.Id);
        }
    }
}
