﻿
namespace BlogModel.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Transactions
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; } = DateTime.UtcNow;
        public int Type { get; set; }
        public decimal Balance { get; set; }
        public int UsersId { get; set; }
    }
}