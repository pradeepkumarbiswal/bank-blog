﻿namespace BlogModel
{
    using BlogModel.Models;
    using Microsoft.EntityFrameworkCore;

    public class BankDbContext : DbContext
    {
        public BankDbContext(DbContextOptions<BankDbContext> options) : base(options)
        {

        }

        public DbSet<Users> Users { get; set; }
        public DbSet<Transactions> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Transactions>()
                .Property(a => a.Balance)
                .HasColumnType("decimal(18,2)");
        }
    }
}
