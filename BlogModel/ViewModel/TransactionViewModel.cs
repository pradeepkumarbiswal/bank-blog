﻿
namespace BlogModel.ViewModel
{
    public class TransactionViewModel
    {
        public int Type { get; set; }
        public decimal Balance { get; set; }
    }
}
