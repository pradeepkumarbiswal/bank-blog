﻿namespace BlogModel.ViewModel
{
    using static BlogModel.Utils.TransactionEnum;

    public class TransactionReturnViewModel
    {
        public TransactionType TransactionType { get; set; }
        public string Errors { get; set; }
        public bool Success { get; set; }
    }
}
