﻿
namespace BlogModel.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    public class TransactionEnum
    {
        public enum TransactionType
        {
            Deposit = 0,
            Withdrawal = 1
        }

        public TransactionType GetTransactionType(int Type)
        {
            return (TransactionType)Type;
        }
    }
}
