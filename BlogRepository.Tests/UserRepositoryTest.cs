namespace BlogRepository.Tests
{
    using BlogModel.Models;
    using BlogModel.ViewModel;
    using BlogServices.User;
    using FakeItEasy;
    using SharedTestData;
    using System;
    using System.Threading.Tasks;
    using Xunit;
    public class UserRepositoryTest
    {
        private readonly IUserRepository _fakeRepository;
        private readonly int userId =1;
        public UserRepositoryTest()
        {
            _fakeRepository = A.Fake<IUserRepository>();
        }
        [Fact]
        public async Task CreateUser_Test()
        {

            //Arrange
            var data = new TestData().Userdata();
            A.CallTo(() => _fakeRepository.CreateUserAsyncRepo(A<Users>.Ignored)).Returns(data);

            //Act
            var addUser = await _fakeRepository.CreateUserAsyncRepo(data);

            //Assert
            Assert.Equal(addUser, data);

        }

        [Fact]
        public async Task LoginUser_Test()
        {

            //Arrange
            var data = new TestData().Userdata();
            A.CallTo(() => _fakeRepository.LoginUserAsyncRepo(A<LoginViewModel>.Ignored)).Returns(data);

            //Act
            var loginUser = await _fakeRepository.LoginUserAsyncRepo(new LoginViewModel {Email="pkbiswal@gmail.com",Password="12345" });

            //Assert
            Assert.Equal(loginUser, data);

        }

        [Fact]
        public async Task GetUserWithTransaction_Test()
        {

            //Arrange
            var data = new TestData().UserWithTransaction();
            A.CallTo(() => _fakeRepository.GetUserWithTransactionsRepo(A<int>.Ignored)).Returns(data);

            //Act
            var userWithTransaction = await _fakeRepository.GetUserWithTransactionsRepo(userId);

            //Assert
            Assert.Equal(userWithTransaction.Id, data.Id);

        }
    }
}
