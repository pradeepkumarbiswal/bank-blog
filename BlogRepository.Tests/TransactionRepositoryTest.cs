﻿namespace BlogRepository.Tests
{
    using BlogModel.Models;
    using BlogServices.Transaction;
    using FakeItEasy;
    using SharedTestData;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Xunit;

    public class TransactionRepositoryTest
    {
        private readonly ITransactionRepository _fakeRepository;
        private readonly int userId = 1;

        public TransactionRepositoryTest()
        {
            _fakeRepository = A.Fake<ITransactionRepository>();
        }

        [Fact]
        public async Task GetAllTransaction_Test()
        {
            //Arrange
            var data = new TestData().TransactionData();
            A.CallTo(() => _fakeRepository.GetAllTransactionAsyncRepo(A<int>.Ignored)).Returns(data);

            //Act
            var transactionData = await _fakeRepository.GetAllTransactionAsyncRepo(userId);

            //Assert
            transactionData.Equals(data);
        }

        [Fact]
        public async Task GetCurrentBalance_Test()
        {
            //Arrange
            var data = new TestData().TransactionData();
            A.CallTo(() => _fakeRepository.GetCurrentBalanceAsyncRepo(A<int>.Ignored)).Returns(data[0]);

            //Act
            var transactionData = await _fakeRepository.GetCurrentBalanceAsyncRepo(userId);

            //Assert
            Assert.Equal(transactionData, data[0]);
        }

        [Fact]
        public async Task DepositeWithdraw_Test()
        {
            //Arrange
            var data = new TestData().TransactionData();
            A.CallTo(() => _fakeRepository.DepositeWithdrawAsyncRepo(A<Transactions>.Ignored)).Returns(data[0]);

            //Act
            var transactionData = await _fakeRepository.DepositeWithdrawAsyncRepo(data[0]);

            //Assert
            Assert.Equal(transactionData, data[0]);
        }
    }
}
