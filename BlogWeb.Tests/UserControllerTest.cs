namespace BlogWeb.Tests
{
    using Blog.Controllers;
    using BlogModel.Models;
    using BlogModel.ViewModel;
    using BlogServices.User;
    using FakeItEasy;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using SharedTestData;
    using Xunit;
    public class UserControllerTest
    {
        private readonly IUserService _fakeServices;
        private readonly UserController _userController;
        private readonly ApplicationSettings appSettings = new ApplicationSettings() { JWT_Secret = "banktransactionsecretkey" };

        public UserControllerTest()
        {
            IOptions<ApplicationSettings> _options = Options.Create(appSettings);
            _fakeServices = A.Fake<IUserService>();

            _userController = new UserController(_fakeServices, _options)
            {
                ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() }
            };
            _userController.ControllerContext.HttpContext.Request.Headers["authorization"] = "Bearer token";

        }


        [Fact]
        public void CreateUser_Ok_Test()
        {
            //Arrange
            TestData testData = new TestData();
            var data = testData.CreateProfile();
            A.CallTo(() => _fakeServices.CreateUserAsync(A<Users>.Ignored)).Returns(testData.Userdata());

            //Act
            var response = _userController.Register(data).Result;
            //Assert
            var okResult = response as OkObjectResult;
            Assert.Equal(StatusCodes.Status200OK, okResult.StatusCode);
        }


        [Fact]
        public void CreateUser_Model_BadRequest_Test()
        {
            //Arrange
            TestData testData = new TestData();
            A.CallTo(() => _fakeServices.CreateUserAsync(A<Users>.Ignored)).Returns(testData.Userdata());

            //Act
            var response = _userController.Register(null).Result;
            //Assert
            var okResult = response as BadRequestObjectResult;
            Assert.Equal(StatusCodes.Status400BadRequest, okResult.StatusCode);
        }

        [Fact]
        public void CreateUser_Return_Null_BadRequest_Test()
        {
            //Arrange
            TestData testData = new TestData();
            var data = testData.CreateProfile();
            Users users = null;
            A.CallTo(() => _fakeServices.CreateUserAsync(A<Users>.Ignored)).Returns(users);

            //Act
            var response = _userController.Register(data).Result;
            //Assert
            var okResult = response as BadRequestObjectResult;
            Assert.Equal(StatusCodes.Status400BadRequest, okResult.StatusCode);
        }

        [Fact]
        public void Login_Ok_Test()
        {
            //Arrange
            TestData testData = new TestData();
            A.CallTo(() => _fakeServices.LoginUserAsync(A<LoginViewModel>.Ignored)).Returns(testData.Userdata());

            //Act
            var response = _userController.Login(new LoginViewModel { Email = "pkbiswal321@gmail.com", Password = "12345" }).Result;
            //Assert
            var okResult = response as OkObjectResult;
            Assert.Equal(StatusCodes.Status200OK, okResult.StatusCode);
        }
    }
}
