﻿namespace BlogWeb.Tests
{
    using Blog.Controllers;
    using BlogModel.Models;
    using BlogServices.Transaction;
    using FakeItEasy;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using SharedTestData;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Xunit;

    public class TransactionControllerTest
    {
        private readonly ITransactionService _fakeServices;
        private readonly TransactionController _userController;
        private readonly int userId = 1;

        public TransactionControllerTest()
        {
            _fakeServices = A.Fake<ITransactionService>();

            _userController = new TransactionController(_fakeServices)
            {
                ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() }
            };
            _userController.ControllerContext.HttpContext.Request.Headers["authorization"] = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOiIxIiwibmJmIjoxNjE2NzAxNDY2LCJleHAiOjE2MjQ0Nzc0NjMsImlhdCI6MTYxNjcwMTQ2Nn0.CVwyG_0ctZA7TdP8wIAvvdy2vSTbLmkNAdHI4xccjJA";
        }

        [Fact]
        public void GetAllTransaction_Ok_Test()
        {
            //Arrange
            TestData testData = new TestData();
            var data = testData.TransactionData();
            A.CallTo(() => _fakeServices.GetAllTransactionAsync(A<int>.Ignored)).Returns(data);

            //Act
            var response = _userController.GetAllTransaction(userId).Result;
            //Assert
            var okResult = response as OkObjectResult;
            Assert.Equal(StatusCodes.Status200OK, okResult.StatusCode);
        }

        [Fact]
        public void GetAllTransaction_Null_BadRequest_Test()
        {
            //Arrange
            List<Transactions> transactions = new List<Transactions>();
            A.CallTo(() => _fakeServices.GetAllTransactionAsync(A<int>.Ignored)).Returns(transactions);

            //Act
            var response = _userController.GetAllTransaction(userId).Result;

            //Assert
            var okResult = response as BadRequestObjectResult;
            Assert.Equal(StatusCodes.Status400BadRequest, okResult.StatusCode);
        }

    }
}
