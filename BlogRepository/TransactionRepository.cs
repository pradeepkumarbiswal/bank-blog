﻿
namespace BlogRepository
{
    using BlogModel;
    using BlogModel.Models;
    using BlogServices.Transaction;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class TransactionRepository : ITransactionRepository
    {
        private readonly BankDbContext _bankDbContext;

        public TransactionRepository(BankDbContext bankDbContext)
        {
            _bankDbContext = bankDbContext;
        }
        public async Task<Transactions> DepositeWithdrawAsyncRepo(Transactions transactions)
        {
            await _bankDbContext.Transactions.AddAsync(transactions);
            await _bankDbContext.SaveChangesAsync();
            return transactions;
        }

        public async Task<IEnumerable<Transactions>> GetAllTransactionAsyncRepo(int userId)
        {
            return await _bankDbContext.Transactions
                .Where(x => x.UsersId == userId)
                .ToListAsync();
        }

        public async Task<Transactions> GetCurrentBalanceAsyncRepo(int userId)
        {
            return await _bankDbContext.Transactions
                .Where(x => x.UsersId == userId)
                .OrderByDescending(x => x.Date)
                .FirstOrDefaultAsync();
        }
    }
}
