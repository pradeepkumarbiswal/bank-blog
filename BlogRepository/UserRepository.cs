﻿namespace BlogRepository
{
    using BlogModel;
    using BlogModel.Models;
    using BlogModel.ViewModel;
    using BlogServices.User;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Threading.Tasks;

    public class UserRepository : IUserRepository
    {
        private readonly BankDbContext _bankDbContext;

        public UserRepository(BankDbContext bankDbContext)
        {
            _bankDbContext = bankDbContext;
        }

        public async Task<Users> CreateUserAsyncRepo(Users users)
        {
            await _bankDbContext.Users.AddAsync(users);
            await _bankDbContext.SaveChangesAsync();
            return users;
        }

        public async Task<Users> GetUserProfile(string email)
        {
            return await _bankDbContext.Users.Where(x => x.Email == email).FirstOrDefaultAsync();
        }

        public async Task<Users> GetUserWithTransactionsRepo(int userId)
        {
            return await _bankDbContext.Users.Include(x => x.Transactions).FirstOrDefaultAsync(x => x.Id == userId);
        }

        public async Task<Users> LoginUserAsyncRepo(LoginViewModel loginViewModel)
        {
            return await _bankDbContext.Users
                .FirstOrDefaultAsync(x => x.Email == loginViewModel.Email && x.Password == loginViewModel.Password);
        }
    }
}
