﻿
namespace BlogServices.Imp
{
    using BlogModel.Models;
    using BlogModel.Utils;
    using BlogServices.Transaction;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _transactionRepository;

        public TransactionService(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }
        public async Task<Transactions> DepositeWithdrawAsync(int userId, decimal amount, TransactionEnum.TransactionType transactionType)
        {
            Transactions transactions = new Transactions();

            var currentTransaction = await _transactionRepository.GetCurrentBalanceAsyncRepo(userId);
            var balance = currentTransaction?.Balance ?? 0;

            if (transactionType == TransactionEnum.TransactionType.Deposit)
            {
                amount = balance + amount;
            }
            else
            {
                amount = balance - amount;
            }

            if (amount < 0)
            {
                return transactions;
            }

            transactions.Balance = amount;
            transactions.UsersId = userId;
            transactions.Type = Convert.ToInt32(transactionType);

            return await _transactionRepository.DepositeWithdrawAsyncRepo(transactions);

        }

        public async Task<IEnumerable<Transactions>> GetAllTransactionAsync(int userId)
        {
            return await _transactionRepository.GetAllTransactionAsyncRepo(userId);
        }

        public async Task<Transactions> GetCurrentBalanceAsync(int userId)
        {
            return await _transactionRepository.GetCurrentBalanceAsyncRepo(userId);
        }
    }
}
