﻿namespace BlogServices.Imp
{
    using BlogModel.Models;
    using BlogModel.ViewModel;
    using BlogServices.User;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class UserService:IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }


        public async Task<Users> CreateUserAsync(Users users)
        {
            var userDetails = await _userRepository.GetUserProfile(users.Email);

            if(userDetails?.Email != null)
            {
                return null;
            }
            return await _userRepository.CreateUserAsyncRepo(users);
        }

        public async Task<Users> GetUserWithTransactions(int userId)
        {
            return await _userRepository.GetUserWithTransactionsRepo(userId);
        }

        public async Task<Users> LoginUserAsync(LoginViewModel loginViewModel)
        {
            return await _userRepository.LoginUserAsyncRepo(loginViewModel);
        }
    }
}
