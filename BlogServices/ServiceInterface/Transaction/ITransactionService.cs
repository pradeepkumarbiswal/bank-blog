﻿namespace BlogServices.Transaction
{
    using BlogModel.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using static BlogModel.Utils.TransactionEnum;

    public interface ITransactionService
    {
        public Task<IEnumerable<Transactions>> GetAllTransactionAsync(int userId);
        public Task<Transactions> DepositeWithdrawAsync(int userId, decimal amount, TransactionType transactionType);
        public Task<Transactions> GetCurrentBalanceAsync(int userId);
    }
}
