﻿namespace BlogServices.User
{
    using BlogModel.Models;
    using BlogModel.ViewModel;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IUserService
    {
        public Task<Users> CreateUserAsync(Users users);
        public Task<Users> LoginUserAsync(LoginViewModel loginViewModel);
        public Task<Users> GetUserWithTransactions(int userId);
    }
}
