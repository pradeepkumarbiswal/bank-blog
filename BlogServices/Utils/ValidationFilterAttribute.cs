﻿namespace BlogServices.Utils
{
    using BlogModel;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;
    using System;
    using System.Linq;
    public class ValidationFilterAttribute : IActionFilter
    {
        private readonly BankDbContext _dbContext;

        public ValidationFilterAttribute(BankDbContext context)
        {
            _dbContext = context;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                var userId = context.HttpContext.User.FindFirst(claim => claim.Type == "UserId")?.Value;
                if (userId == null)
                {
                    context.Result = new BadRequestObjectResult(new { message = "Invalid or Expired Token, Try again" });
                    return;
                }
                var user = _dbContext.Users.Where(u => u.Id == Convert.ToInt64(userId)).FirstOrDefault();
                if (user == null)
                {
                    context.Result = new BadRequestObjectResult(new { message = "Invalid User, Try again" });
                    return;
                }
                context.ActionArguments.TryGetValue("userId", out object UserId);

                if (!context.ActionArguments.ContainsKey("UserId"))
                {
                    context.ActionArguments.Add("userId", Convert.ToInt32(userId));
                }
                else
                {
                    context.ActionArguments.Add("userId", Convert.ToInt32(userId));
                }
                //if (context.ActionArguments.ContainsKey("UserId"))
                //{
                //    context.ActionArguments.Add("userId", Convert.ToInt64(userId));
                //}

            }
            catch (Exception ex)
            {
                context.Result = new BadRequestObjectResult(new { message = "Invalid User, Try again", ex });
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // Method intentionally left empty.
        }
    }
}
