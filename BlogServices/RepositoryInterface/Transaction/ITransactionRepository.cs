﻿namespace BlogServices.Transaction
{
    using BlogModel.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using static BlogModel.Utils.TransactionEnum;

    public interface ITransactionRepository
    {
        public Task<IEnumerable<Transactions>> GetAllTransactionAsyncRepo(int userId);
        public Task<Transactions> DepositeWithdrawAsyncRepo(Transactions transactions);
        public Task<Transactions> GetCurrentBalanceAsyncRepo(int userId);
    }
}
