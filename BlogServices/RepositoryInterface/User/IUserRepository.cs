﻿namespace BlogServices.User
{
    using BlogModel.Models;
    using BlogModel.ViewModel;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IUserRepository
    {
        public Task<Users> CreateUserAsyncRepo(Users users);
        public Task<Users> LoginUserAsyncRepo(LoginViewModel loginViewModel);
        public Task<Users> GetUserWithTransactionsRepo(int userId);
        public Task<Users> GetUserProfile(string email);
    }
}
