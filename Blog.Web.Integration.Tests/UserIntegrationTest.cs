namespace Blog.Web.Integration.Tests
{
    using Microsoft.AspNetCore.Http;
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Xunit;
    public class UserIntegrationTest: IClassFixture<TestFixture<Startup>>
    {
        private readonly HttpClient Client;

        public UserIntegrationTest(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
        }

        [Fact]
        public async Task CreateUser_Return_Ok_Test()
        {
            // Arrange
            var request = new
            {
                Url = "/api/User/Register",
                Body = new
                {
                    UserName = "pkbiswal",
                    Email = "pkbiswal5@hmail.com",
                    Password = "12345",
                    ConfirmPassword = "12345"
                }
            };

            // Act
            var response = await Client.PostAsync(request.Url, ContentHelper.GetStringContent(request.Body));
            var value = await response.Content.ReadAsStringAsync();

            // Assert
            response.EnsureSuccessStatusCode();
        }
        [Fact]
        public async Task CreateUser_Return_Duplicate_User_BadRequest_Test()
        {
            // Arrange
            var request = new
            {
                Url = "/api/User/Register",
                Body = new
                {
                    UserName = "pkbiswal",
                    Email = "pkbiswal5@hmail.com",
                    Password = "12345",
                    ConfirmPassword = "12345"
                }
            };

            // Act
            var response = await Client.PostAsync(request.Url, ContentHelper.GetStringContent(request.Body));
            var value = await response.Content.ReadAsStringAsync();

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, response.StatusCode.GetHashCode());

            //response.StatusCode.Equals(400);
        }
    }
}
