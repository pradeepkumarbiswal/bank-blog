﻿namespace SharedTestData
{
    using FakeItEasy;
    using Microsoft.EntityFrameworkCore;
    using MockQueryable.FakeItEasy;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    [ExcludeFromCodeCoverage]
    public static class FakeDbContext
    {
        public static T Fake<T>() where T : DbContext
        {
            return A.Fake<T>(x => x.WithArgumentsForConstructor(new[] { new DbContextOptions<T>() }));
        }

        public static void MockData<TContext, TEntity>(this TContext context, List<TEntity> data, Expression<Func<DbSet<TEntity>>> expression) where TContext : DbContext where TEntity : class
        {
            var mock = data.AsQueryable().BuildMockDbSet();
            A.CallTo(expression).Returns(mock);
            A.CallTo(() => mock.Add(A<TEntity>._)).Invokes(x => data.Add(x.Arguments[0] as TEntity));
            A.CallTo(mock).Where(x => x.Method.Name == "AddRangeAsync").WithReturnType<Task>().ReturnsLazily(x => Task.Run(() => data.AddRange(x.Arguments.First() as IEnumerable<TEntity>)));
        }
    }
}
