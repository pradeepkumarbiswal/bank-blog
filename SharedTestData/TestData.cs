﻿
namespace SharedTestData
{
    using BlogModel.Models;
    using BlogModel.ViewModel;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Threading.Tasks;

    [ExcludeFromCodeCoverage]
    public class TestData
    {
        public async Task<Users> GetUserProfile(int userId)
        {
            var userDeatails = new Users
            {
                Id = userId,
                UserName = "pkbiswal",
                Email = "pkbiswal2@hmail.com",
                Password = "12345",
                CreateDateTime = DateTime.UtcNow,
                Transactions = null
            };
            return await Task.FromResult(userDeatails);
        }
        public UserViewModel CreateProfile()
        {
            var registerDeatails = new UserViewModel
            {
                UserName = "pkbiswal",
                Email = "pkbiswal2@hmail.com",
                Password = "12345",
                ConfirmPassword = "12345",
            };
            return registerDeatails;
        }

        public Users Userdata()
        {
            var data = new Users
            {
                Id = 1,
                UserName = "pkbiswal",
                Email = "pkbiswal2@hmail.com",
                Password = "12345",
                CreateDateTime = DateTime.UtcNow,
                Transactions = null
            };
            return data;
        }

        public async Task<Users> UserWithTransaction()
        {
            var data = new Users
            {
                Id = 1,
                UserName = "pkbiswal",
                Email = "pkbiswal2@hmail.com",
                Password = "12345",
                CreateDateTime = DateTime.UtcNow,
                Transactions = new List<Transactions>()
                {
                    new Transactions
                    {
                        Id=1,
                        Balance=100,
                        Type=1,
                        Date=DateTime.Now,
                        UsersId=1
                    },
                    new Transactions
                    {
                        Id=2,
                        Balance=100,
                        Type=0,
                        Date=DateTime.Now,
                        UsersId=1
                    }
                }
            };
            return await Task.FromResult(data);
        }

        public Transactions[] TransactionData()
        {
            var transactions = new[]
             {
                new Transactions
                {
                    Id=1,
                    Balance=100,
                    Type=1,
                    Date=DateTime.Now,
                    UsersId=1
                },
                new Transactions
                {
                    Id=2,
                    Balance=100,
                    Type=0,
                    Date=DateTime.Now,
                    UsersId=1
                }
            };


            return transactions;
        }
    }

}
