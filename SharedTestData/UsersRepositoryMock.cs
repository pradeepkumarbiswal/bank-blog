﻿
namespace SharedTestData
{
    using BlogModel;
    using BlogModel.Models;
    using BlogRepository;
    using BlogServices.User;
    using FakeItEasy;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    public class UsersRepositoryMock
    {
        public static IUserRepository CreateUserRepository()
        {
            IDbContextFactory<BankDbContext> dbContextFactory = A.Fake<IDbContextFactory<BankDbContext>>();
            BankDbContext dbContext = FakeDbContext.Fake<BankDbContext>();
            A.CallTo(() => dbContextFactory.CreateDbContext()).Returns(dbContext);

            var data = new List<Users>
            {
                new Users {
                Id = 1,
                UserName = "pkbiswal",
                Email = "pkbiswal@hmail.com",
                Password = "12345",
                CreateDateTime = DateTime.UtcNow,
                },
                new Users {
                Id = 2,
                UserName = "pkbiswal",
                Email = "pkbiswal1@hmail.com",
                Password = "12345",
                CreateDateTime = DateTime.UtcNow,
                }
            }.AsQueryable();


            dbContext.MockData(data.ToList(), () => dbContext.Users);

            return new UserRepository(dbContextFactory.CreateDbContext());
        }

    }
}
